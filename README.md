# Arcade Jo 2024


## Description
Nous avons choisi avec mes camarades, pour ce projet de travailler sur le thème des JO de Paris 2024. C’est un choix en accord avec l’actualité. Ainsi ce projet se décompose en 3 parties principales, d’une part un décompte qui se terminera lors de la cérémonie d’ouverture. Secondement il y aura une data base contenant les sportifs qui ont marqué l’histoire du sport français. Pour finir, un menu contenant une multitude de mini-jeux sur le thème du sport seront disponibles. 

## Guide d'utilisation du projet 
Pour lancer notre projet il faudra lancer le fichier projet.py qui correspond à l'accueil. 
Il faudra aussi installer les bibliothèques précisées dans le requirements.txt au préalable. 
Pour installer les dépendances répertoriées dans le fichier requirements.txt, vous pouvez utiliser la commande:
    pip install -r requirements.txt

## Guide des touches 
Pour le mini jeu de combat de boxe, il faut appuyer sur la barre d'espace pour faire combattre les deux boxeurs. 
Pour le mini jeu de course il faut cliquer le plus vite possible avec la souris.
Pour le mini jeu de football il faut cliquer avec le clic gauche pour tirer à gauche, la molette pour tirer au centre, le clic droit pout tirer à droite. 
Pour le reste le quiz et la base de données histoire des JO il faut cliquer sur les boutons avec la souris. 

## Sources des images (& musiques)
Les images que nous avons utilisé:
- les fond d'écrans ont été pour beaucoup générés par l'IA Midjourney
- les sprite du coureur ont été trouvé sur https://www.gameart2d.com/freebies.html
- le reste des images et musiques sont placés sous licence Creative Commons. 

## Bibliothèques & modules utilisés
- pygame
- sqlite3
- os
- sys 
- datetime
- random 






