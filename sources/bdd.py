#importation modules
import pygame
import os
import sqlite3
import sys

# Si vous encontrez un problème de connexion entre les modules modifiez la ligne ci-dessous avec l'adresse du répertoire où vous avez enregistré le projet 
#os.chdir("U:\Documents\projet_concours\projet concours")

pygame.init()

#Définition des couleurs utilisées
police = pygame.font.Font(None, 25)  #Définition de la police avec la taille 32

#Définition des couleurs utilisées dans le jeu
couleur_texte_bouton = (217, 226, 237)
couleur_texte = (255, 255, 255)
couleur_pv = (255, 0, 0)
couleur_barre = (0, 255, 0)
couleur_bouton = (179, 172, 98)
couleur_case = (187, 195, 204)

#Définition de la classe MenuDeroulant
class MenuDeroulant:
    def __init__(self, x, y, largeur, hauteur, options, titre):
        #Initialisation de la classe avec des paramètres
        self.rect = pygame.Rect(x, y, largeur, hauteur)  #Crée un rectangle définissant la zone du menu déroulant.
        self.options = options  #Stocke la liste d'options du menu déroulant.
        self.ouvert = False  #Initialise l'état du menu déroulant comme fermé.
        self.selection = options[0]  #Initialise la sélection avec la première option de la liste.
        self.titre = titre  #Stocke le titre du menu déroulant.

    #Méthode pour afficher le menu déroulant
    def afficher(self, ecran):
        """Dans la méthode afficher le self fait référence à l'instance de la classe à laquelle la méthode est associée, en permettant d’accéder aux attributs et méthodes de cette instance.
        Écran représente la surface d'affichage de pygame sur laquelle les éléments graphiques sont dessinés. La méthode sert à afficher le menu graphique du jeu codé sur l’écran,
        en utilisant les propriétés du programme pour déterminer la position, la taille et le contenu du menu."""
        pygame.draw.rect(ecran, couleur_bouton, self.rect)  #Dessine un rectangle pour afficher le menu.
        texte_menu = police.render(self.titre, True, couleur_texte_bouton)  #Affiche le titre du menu.
        ecran.blit(texte_menu, (self.rect.x + 10, self.rect.y + 10))

        if self.ouvert:
            for i in range(len(self.options)):
                y_offset = (i + 1) * 30 + 10  #Calcule la position verticale des options.
                option_rect = pygame.Rect(self.rect.x, self.rect.y + y_offset, self.rect.width, 30)  #Crée un rectangle pour chaque option.
                pygame.draw.rect(ecran, couleur_case, option_rect)  #Dessine le rectangle de l'option.
                texte_option = police.render(self.options[i], True, couleur_texte)  #Affiche le texte de l'option.
                ecran.blit(texte_option, (option_rect.x + 10, option_rect.y + 10))  #Positionne le texte de l'option.
            
    #Méthode pour gérer un clic sur le menu déroulant
    def gerer_clic(self, event):
        """Pour méthode « gerer_clic » est celui consiste a vérifier les clics de souris, dans le menu déroulant. le paramètre Self fait référence à la classe qui etait associée au dessus.
        Le paramètre "event" il représente  le moment du clic avec la souris. Cela vérifie si un clic de souris est appuyer dans le menu déroulant. Si c'est le cas, elle s’ouvre. 
        Si le menu est ouvert et que le clic de souris n'est pas dans la zone du menu principal, la méthode traverse le menu pour vérifier si l'une d'entre elles est cliquée."""
        if event.type == pygame.MOUSEBUTTONDOWN:
            if self.rect.collidepoint(event.pos):
                self.ouvert = not self.ouvert  #Ouvre ou ferme le menu en fonction de son état actuel.
            elif self.ouvert:
                for i in range(len(self.options)):
                    y_offset = (i + 1) * 30  #Calcule la position verticale des options.
                    option_rect = pygame.Rect(self.rect.x, self.rect.y + y_offset, self.rect.width, 30) #Crée un rectangle pour chaque option.
                    if option_rect.collidepoint(event.pos):
                        self.selection = self.options[i]  #Met à jour la sélection avec l'option cliquée.
                        self.ouvert = False  #Ferme le menu déroulant.
                        self.titre = self.selection  #Met à jour le titre avec la sélection.



#taille fenetre
largeur, hauteur = (800, 600)
taille_fenetre = (largeur, hauteur)

#création fenetre
fenetre = pygame.display.set_mode(taille_fenetre)
pygame.display.set_caption("PARIS 2024")
image = pygame.image.load("images/tour_eiffel_coureur.png").convert()
pygame.display.set_icon(image)



def creer_table_athletes():
    """
    cette fonction a servi a créer la base de données, maintenant il ne faut plus l'utiliser car il y aurait une erreur de doublons 
    """
#Connexion à la db
    connection = sqlite3.connect('base_de_donnees.db')
    c = connection.cursor()

    #Création de la table AthleteFrancais
    c.execute('''CREATE TABLE IF NOT EXISTS AthleteFrancais (
                Nom TEXT PRIMARY KEY,
                Prenom TEXT,
                MedailleOr INTEGER,
                MedailleArgent INTEGER,
                MedailleBronze INTEGER,
                Sport TEXT,
                Description TEXT,
                PRIMARY KEY(Nom,Prenom));
                ''')
    

    #Liste des données des athlètes fr
    athletes = [
    ('Teddy Riner', 'Riner', 2, 1, 0, 'Judo', 'Athlète français, spécialiste du judo.'),
    ('Florent Manaudou', 'Manaudou', 1, 3, 2, 'Natation', 'Nageur français, champion olympique et recordman du monde.'),
    ('Julian Alaphilippe', 'Alaphilippe', 3, 2, 1, 'Cyclisme sur piste', 'Cycliste français, multiple médaillé olympique.'),
    ('Mélanie De Jesus Dos Santos', 'De Jesus Dos Santos', 2, 1, 2, 'Gymnastique artistique', 'Gymnaste française, championne du monde et d\'Europe.'),
    ('Caroline Garcia', 'Garcia', 1, 0, 1, 'Tennis', 'Joueuse de tennis française, ancienne numéro 4 mondiale.'),
    ('Marie-Florence Candassamy', 'Candassamy',0, 0, 1, 'Escrime', 'Escrimeuse française, Championne du monde d\'escrime.'),
    ('Jean-Charles Valladont', 'Valladont', 0, 1, 0, 'Tir à l\'arc', 'Tireur français, Ancien vice champion olypique de tir à l\'arc.'),
    ('Kévin Mayer', 'Mayer', 1, 0, 0, 'Décathlon ', 'Athlète français, spécialiste et grand champion du décathlon.'),
    ('Victor Wembanyama', 'Wembanyama', 1, 0, 0, 'Basket', 'Basketteur française, Grand Talent reconnu du basket français et de la NBA.')
]

    #Insertion des données dans la table AthleteFrancais
    c.executemany('''INSERT INTO AthleteFrancais VALUES (?, ?, ?, ?, ?, ?, ?)''', athletes)

    #déconnexion
    connection.commit()
    connection.close()


def accueil():
    #Définition des images 
    bouton_retour = pygame.Rect(0, 0, 100, 100)
    retour = pygame.image.load("images/retour1.png").convert_alpha()
    retour = pygame.transform.scale(retour, (100, 100))
    fond = pygame.image.load("images/tour_eiffel_coureur.png").convert()
    fond = pygame.transform.scale(fond, (largeur, hauteur))
    
    # connextion à la base de donnée
    connection = sqlite3.connect('base_de_donnees.db')
    c = connection.cursor()
    # on récupère les noms des atlètes dans une liste 
    c.execute("SELECT Nom FROM AthleteFrancais")
    tab= c.fetchall()
    tab2= []
    for i in tab:
        tab2.append(i[0])
    
    # on crée le menu déroulant avec les noms 
    menu = MenuDeroulant(150, 15, 330, 50, tab2, "Athlète Français")
    
    bouton_start = pygame.Rect(550, 15, 150, 50)
    resultat = ""
    choix = False
    
    # boucle principale 
    continuer=True
    while continuer:
        fenetre.blit(fond, (0,0))
        fenetre.blit(retour, (0,0))    
        
                
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                # si on apuie sur start, on va afficher les informations 
                if bouton_start.collidepoint(event.pos):
                    athletes_choisie = menu.selection
                    data = (athletes_choisie,)
                    c.execute("SELECT * FROM AthleteFrancais WHERE Nom = ?", data)
                    resultat = c.fetchall()
                    if resultat:
                        choix = True
                        
                if menu.rect.collidepoint(event.pos):
                    menu.ouvert = not menu.ouvert
                    if menu.ouvert:
                        choix = False 
                        
                elif menu.ouvert:
                    #Traite le clic dans le menu déroulant
                    menu.gerer_clic(event)
                
                # bouton retour en arriere 
                if bouton_retour.collidepoint(event.pos):
                    return 0
                    
            elif event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()  

           
        menu.afficher(fenetre)
        pygame.draw.rect(fenetre, couleur_bouton, bouton_start)  #Faire un rectangle
        texte_start = police.render("Start", True, couleur_texte_bouton)  #Crée un texte "Start" 
        fenetre.blit(texte_start, (600, 26))
        
        # si on a cliqué sur start on affiche :
        if choix == True:
            texte = ["son nombre de medaille d'or: " , "son nombre de medaille d'argent: ", "son nombre de medaille de bronze: ", "sa discipline: ", "sa description: "]
            for i in range(5):
                stat = texte[i] + str(resultat[0][2 + i])
                texte_resultat = police.render(str(stat), True, couleur_texte)
                fenetre.blit(texte_resultat, (10, 400 + i*30))  
        else:
            for i in range(5):
                stat = "            "     
                texte_resultat = police.render(str(stat), True, couleur_texte)  
                fenetre.blit(texte_resultat, (10, 400 + i*30))  
        
        #Rafraichir l'affichage
        pygame.display.flip()

    pygame.quit()
    sys.exit()
    # on ferme la base de donnée
    connection.commit()
    connection.close()

