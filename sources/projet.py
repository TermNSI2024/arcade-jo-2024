#importation modules
import pygame
import os
from datetime import datetime
from random import *
import time
import ballon
import bdd
import Quiz

# Si vous encontrez un problème de connexion entre les modules modifiez la ligne ci-dessous avec l'adresse du répertoire où vous avez enregistré le projet 
#os.chdir("U:\Documents\projet_concours\projet concours")

pygame.init()

#taille fenetre
largeur, hauteur = (800, 600)
taille_fenetre = (largeur, hauteur)

#couleurs
blanc = (255, 255, 255)
noir = (0, 0, 0)
rouge = (255, 0, 0)
vert = (0, 255, 0)
bleu = (0, 0, 255)

#polices
police = pygame.font.Font(None, 50)



#création fenetre
fenetre = pygame.display.set_mode(taille_fenetre)
pygame.display.set_caption("PARIS 2024")
image = pygame.image.load("images/accueil.png").convert()
pygame.display.set_icon(image)
background_image = "images/image_fond.png"


# classe pour les questions du quiz 
class Question:
    def __init__(self, question, reponses, reponse_correcte):
        self.question = question
        self.reponses = reponses
        self.reponse_correcte = reponse_correcte

questions_list = [
    "Comment appelle-t-on les sportifs participant aux Jeux Olympiques?",
    "Quelle innovation technologique sera introduite pour la première fois aux JO de Paris 2024?       ",
    "Quel sera le lieu emblématique pour les épreuves de natation aux Jeux de 2024?         ",
    "Dans quelle discipline Laure Manaudou et son frère Florent ont-ils remporté des médailles olympiques?      ",
    "Comment appelle-t-on le personnage imaginaire qui symbolise l’esprit des Jeux?   "
]

list_questions_reponses = [
    Question(questions_list[0], ['des athlètes', 'des gymnastes ', ' des musclés', 'des footeux'], 'A'),
    Question(questions_list[1], ['Athlètes robots', 'Éclairage holographique', 'Billets électroniques implantables', 'Suivi en temps réel des médailles via une application mobile'], 'A'),
    Question(questions_list[2], ['Seine River', 'La Villette Canal', 'Bassin de la Villette', 'Piscine de la Tour Eiffel'], 'C'),
    Question(questions_list[3], ['La natation', 'Le judo', 'L’équitation', 'Curling sur glace synthétique'], 'B'),
    Question(questions_list[4], [' La mascotte', 'La cocotte', 'La peluche', 'Les supporters'], 'A'),
]



# Page début avec timer 
def accueil():
    # timer avant date céremonie d'ouverture des jo
    date_jo = datetime(2024, 7, 26, 20, 24, 0)
    clock = pygame.time.Clock()
    fond = pygame.image.load("images/accueil.png").convert()
    fond = pygame.transform.scale(fond, (largeur, hauteur))
    bouton_start = pygame.Rect(350, 550, 200, 50)
    
    
    while datetime.now() < date_jo:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
            
            if event.type == pygame.MOUSEBUTTONDOWN:
                    #si un clic est detecté sur le bouton_start
                    if bouton_start.collidepoint(event.pos):
                        #on lance la page suivante
                        return accueil2()
                
        temps_restant = date_jo - datetime.now()
        jours, secondes = temps_restant.days, temps_restant.seconds
        heures, minutes = divmod(secondes, 3600)
        minutes, secondes = divmod(minutes, 60)

        #compte à rebours:
        texte = str(jours)+" jours, "+str(heures)+" heures, "+str(minutes)+ "minutes, "+str(secondes)+" secondes"
        texte_surface = police.render(texte, True, noir)
        
        #affichage du texte
        texte_rect = texte_surface.get_rect(center=(400, 400))
        fenetre.blit(fond, (0,0))
        fenetre.blit(texte_surface, texte_rect)
        #bouton accueil
        texte_start = police.render("Start", True, noir)
        fenetre.blit(texte_start, (350, 550))
        
        #rafraichir la page
        pygame.display.flip()

        #affichage du chrono
        clock.tick(60)

# Page choix mini jeux ou histoire jo
def accueil2():
    bouton_db = pygame.Rect(450, 350, 260, 45)
    bouton_mj = pygame.Rect(150, 350, 160, 45)
    bouton_retour = pygame.Rect(0, 0, 100, 100)
    texte_db = police.render("Histoire des JO", True, blanc)
    texte_mj = police.render("Mini-jeux", True, blanc)
        
    o=True
    while o==True:
        fond = pygame.image.load("images/img2.png").convert()
        fond = pygame.transform.scale(fond, (largeur, hauteur))
        retour = pygame.image.load("images/retour1.png").convert_alpha()
        retour = pygame.transform.scale(retour, (100, 100))
        for event in pygame.event.get():
            #quitte le programme
            if event.type == pygame.QUIT:
                pygame.quit()
            #bouton pour acceder à la data base
            if event.type == pygame.MOUSEBUTTONDOWN:
                    if bouton_db.collidepoint(event.pos):
                        bdd.accueil()
            #bouton pour acceder aux minis jeux       
            if event.type == pygame.MOUSEBUTTONDOWN:
                    if bouton_mj.collidepoint(event.pos):
                        mj_accueil()
            #bouton pour retourner sur la page avec le compte à rebours       
            if event.type == pygame.MOUSEBUTTONDOWN:
                    if bouton_retour.collidepoint(event.pos):
                        accueil()
                    
        fenetre.blit(fond, (0,0))
        #affichage des boutons
        fenetre.blit(retour, (0,0))
        pygame.draw.rect(fenetre, noir, bouton_db)
        pygame.draw.rect(fenetre, noir, bouton_mj)
        fenetre.blit(texte_db, (450, 350))
        fenetre.blit(texte_mj, (150, 350))
        
        pygame.display.flip()


# Page choix mini jeu 
def mj_accueil():
        bouton_penalty = pygame.Rect(325, 450, 150, 45)
        bouton_course = pygame.Rect(325, 350, 150, 45)
        bouton_combat = pygame.Rect(325, 250, 150, 45)
        bouton_quiz = pygame.Rect(325, 150, 150, 45)
        bouton_retour = pygame.Rect(0, 0, 100, 100)
        texte_penalty = police.render("Football", True, blanc)
        texte_course = police.render("Sprint", True, blanc)
        texte_combat = police.render("Boxe", True, blanc)
        texte_quiz = police.render("Quiz", True, blanc)
        retour = pygame.image.load("images/retour1.png").convert_alpha()
        retour = pygame.transform.scale(retour, (100, 100))
        o=True
        while o==True:
            fond = pygame.image.load("images/jo2024.jpg").convert()
            fond = pygame.transform.scale(fond, (largeur, hauteur))
            for event in pygame.event.get():
           
                if event.type == pygame.QUIT:
                    pygame.quit()
                    
                # lance le jeu penalty
                if event.type == pygame.MOUSEBUTTONDOWN:
                        if bouton_penalty.collidepoint(event.pos):
                            fin = ballon.fenetre_accueil()
                            if fin == 0 :
                                mj_accueil()
                # lance jeu course 
                if event.type == pygame.MOUSEBUTTONDOWN:
                        if bouton_course.collidepoint(event.pos):
                            accueil_course()
                        
                # lance jeu combat boxe
                if event.type == pygame.MOUSEBUTTONDOWN:
                        if bouton_combat.collidepoint(event.pos):
                            accueil_combat()
                
                # lance jeu quiz 
                if event.type == pygame.MOUSEBUTTONDOWN:
                        if bouton_quiz.collidepoint(event.pos):
                            fin1=Quiz.quiz(list_questions_reponses, background_image)
                            if fin1==0:
                                mj_accueil()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if bouton_retour.collidepoint(event.pos):
                         accueil2()
                        
            # affichage images et boutons et texte 
            fenetre.blit(fond, (0,0))
            fenetre.blit(retour, (0,0))
            pygame.draw.rect(fenetre, noir, bouton_penalty)
            pygame.draw.rect(fenetre, noir, bouton_course)
            pygame.draw.rect(fenetre, noir, bouton_combat)
            pygame.draw.rect(fenetre, noir, bouton_quiz)
            fenetre.blit(texte_penalty, (325, 450))
            fenetre.blit(texte_course, (325, 350))
            fenetre.blit(texte_combat, (325, 250))
            fenetre.blit(texte_quiz, (325, 150))
            pygame.display.flip()

# page accueil course avec choix difficulté 
def accueil_course():
    # creation boutons 
    bouton_facile = pygame.Rect(100, 350, 100, 45)
    bouton_moyen = pygame.Rect(325, 350, 115, 45)
    bouton_difficile = pygame.Rect(550, 350, 130, 45)
    bouton_retour = pygame.Rect(0, 0, 100, 100)
    txt_facile= police.render("Facile", True, blanc)
    txt_moyen= police.render("Moyen", True, blanc)
    txt_difficile= police.render("Difficile", True, blanc)
    retour = pygame.image.load("images/retour1.png").convert_alpha()
    retour = pygame.transform.scale(retour, (100, 100))
    
    o=True
    while o==True:
        fond = pygame.image.load("images/img5.jpg").convert()
        fond = pygame.transform.scale(fond, (largeur, hauteur))
        for event in pygame.event.get():
        #quitte le programme
            if event.type == pygame.QUIT:
                o = False 
                pygame.quit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                if bouton_retour.collidepoint(event.pos):
                    mj_accueil()
            if event.type == pygame.MOUSEBUTTONDOWN:
                    # choix jeu facile 
                    if bouton_facile.collidepoint(event.pos):
                        jeu_course(8)
            if event.type == pygame.MOUSEBUTTONDOWN:
                    # choix jeu moyen
                    if bouton_moyen.collidepoint(event.pos):
                        jeu_course(6)
            if event.type == pygame.MOUSEBUTTONDOWN:
                    # choix jeu difficile 
                    if bouton_difficile.collidepoint(event.pos):
                        jeu_course(4)
                        
        # affichage images et boutons et texte 
        fenetre.blit(fond, (0,0))
        fenetre.blit(retour, (0,0))
        pygame.draw.rect(fenetre, noir, bouton_facile)
        pygame.draw.rect(fenetre, noir, bouton_moyen)
        pygame.draw.rect(fenetre, noir, bouton_difficile)
        fenetre.blit(txt_facile, (100, 350))
        fenetre.blit(txt_moyen, (325, 350))
        fenetre.blit(txt_difficile, (550, 350))

        pygame.display.flip()
    
    
# le jeu de course 
def jeu_course(temps):
    bouton_retour = pygame.Rect(0, 0, 100, 100)
    bouton_rejouer = pygame.Rect(660, 0, 200, 40)
    largeur_c=40
    hauteur_c=380
    
    # images animation du courreur 
    images_course = [pygame.image.load("images/run1.png"), pygame.image.load("images/run2.png"), pygame.image.load("images/run3.png"),pygame.image.load("images/run4.png"), pygame.image.load("images/run5.png"),pygame.image.load("images/run6.png"),pygame.image.load("images/run7.png"), pygame.image.load("images/run8.png"), pygame.image.load("images/run9.png"), pygame.image.load("images/run10.png"), pygame.image.load("images/run11.png"),pygame.image.load("images/run12.png"), pygame.image.load("images/run13.png"), pygame.image.load("images/run14.png"), pygame.image.load("images/run15.png")]
    
    # timer pour calculer le temps du joueur 
    index_image_course = 0
    temps_initial = time.time()
    temps_restant = round(time.time()-temps_initial)
    compteur=0
    gagner=False
    continuer=True
    while continuer==True:
        
        while temps_restant<temps:
            # on passe à l'image suivante du personage 
            courreur = images_course[index_image_course]
            index_image_course = (index_image_course + 1) % len(images_course)
            courreur = pygame.transform.scale(courreur, (200, 200))
            
            temps_restant = round(time.time()-temps_initial)
            fond = pygame.image.load("images/img5.jpg").convert()
            fond = pygame.transform.scale(fond, (largeur, hauteur))
            
            for event in pygame.event.get():
                #quitte le programme
                if event.type == pygame.QUIT:
                    pygame.quit()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if bouton_retour.collidepoint(event.pos):
                        continuer=False
                        accueil_course()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if bouton_rejouer.collidepoint(event.pos):
                        accueil_course()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    compteur=compteur+1
                    largeur_c=largeur_c+16
                    
            fenetre.blit(fond, (0,0))
            fenetre.blit(courreur, (largeur_c, hauteur_c))
            
            # si le joueur a passé la ligne d'arrivee 
            if compteur>30:
                gagner=True
                refaire=True
                while refaire==True:
                    
                    # page de fin du jeu 
                    fond = pygame.image.load("images/img2.png").convert()
                    fond = pygame.transform.scale(fond, (largeur, hauteur))
                    texte_victoire = police.render("Victoire !", True, noir)
                    texte_rejouer = police.render("Rejouer", True, blanc)
                    texte_victoire2 = police.render("Vous avez mis moins de: "+str(temps)+"sec", True, noir)
                    
                    for event in pygame.event.get():
                        if event.type == pygame.QUIT:
                            pygame.quit()
                        if event.type == pygame.MOUSEBUTTONDOWN:
                            if bouton_rejouer.collidepoint(event.pos):
                                 accueil_course()
                    
                    fenetre.blit(fond, (0,0))
                    fenetre.blit(texte_victoire, (340, 150))
                    fenetre.blit(texte_victoire2, (150, 490))
                    pygame.draw.rect(fenetre, noir, bouton_rejouer)
                    fenetre.blit(texte_rejouer, (665, 0))
                    
                    pygame.display.flip()
                
            pygame.display.flip()
          
        # si le temps est fini et qu'on a pas atteint la ligne d'arrivé
        if gagner==False:
            continuer=True
            # page de fin du jeu si on a perdu 
            while continuer==True:
                
                fond = pygame.image.load("images/img2.png").convert()
                fond = pygame.transform.scale(fond, (largeur, hauteur))
                texte_victoire = police.render("Perdu !", True, noir)
                texte_rejouer = police.render("Rejouer", True, blanc)
                texte_victoire2 = police.render("Vous devez mettre moins de: "+str(temps)+"sec", True, noir)
                
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        pygame.quit()
                    if event.type == pygame.MOUSEBUTTONDOWN:
                        if bouton_rejouer.collidepoint(event.pos):
                            accueil_course()
                            
                fenetre.blit(fond, (0,0))
                fenetre.blit(texte_victoire, (340, 150))
                fenetre.blit(texte_victoire2, (150, 490))
                pygame.draw.rect(fenetre, noir, bouton_rejouer)
                fenetre.blit(texte_rejouer, (665, 0))
                
                pygame.display.flip()

# Chargement et lancement d'une musique de fond (-1 pour la mettre en continu)
"""pygame.mixer.music.load('musiques/fond.mp3')
pygame.mixer.music.play(-1)"""

#### Paramètres du jeu ###
# Le tour de jeu commencera par le personnage 1
tour_personnage = 1

# Largeur et hauteur de la barre de vie
largeur_barre_max = 100  # Largeur maximale de la barre de vie
hauteur_barre = 20  # Hauteur de la barre de vie

# Liste des arènes disponibles
arenes = ["ring1", "ring2"]  

#### Création des personnages #### (A REMPLACER PAR VOTRE CLASSE)
class Personnage:

  def __init__(self, nom, position_ecr, adresse_chemin_image,
               valeur_points_vie_initial, valeur_points_vie_courant,
               valeur_force):
        self.nom = nom
        self.position_ecr = position_ecr
        self.adresse_chemin_image = adresse_chemin_image
        image = pygame.image.load(adresse_chemin_image).convert_alpha()
        self.nom = police.render(self.nom, True, blanc)
        self.image = pygame.transform.scale(image, (300, 300))
        self.valeur_points_vie_initial = valeur_points_vie_initial
        self.valeur_points_vie_courant = valeur_points_vie_courant
        self.valeur_force = valeur_force
        self.arme = None

  def get_nom(self):
    return self.nom

  def set_nom(self, nom):
    self.nom = nom

  def get_position_ecr(self):
    return self.position_ecr

  def set_position_ecr(self, position_ecr):
    self.position_ecr = position_ecr

  def get_adresse_chemin_image(self):
    return self.adresse_chemin_image

  def set_adresse_chemin_image(self, adresse_chemin_image):
    self.adresse_chemin_image = adresse_chemin_image

  def get_valeur_points_vie_initial(self):
    return self.valeur_points_vie_initial

  def set_valeur_points_vie_initial(self, valeur_points_vie_initial):
    self.valeur_points_vie_initial = valeur_points_vie_initial

  def get_valeur_points_vie_courant(self):
      return self.valeur_points_vie_courant

  def set_valeur_points_vie_courant(self, valeur_points_vie_courant):
    self.valeur_points_vie_courant = valeur_points_vie_courant

  def get_valeur_force(self):
      return self.valeur_force

  def set_valeur_force(self, valeur_force):
      self.valeur_force = valeur_force
      
  def get_image(self):
      return self.image
  
  # pas eu le temps de mettre les armes 
  # def get_arme(self):
  #     return self.arme
  # 
  # def set_arme(self, arme): 
  #     self.arme = arme

  def frappe(self, autre_personnage, puissance):
        # Attaque de base sans arme
        autre_personnage.set_valeur_points_vie_courant(autre_personnage.get_valeur_points_vie_courant() - puissance)


  def est_frappe(self, autre_personnage):
    self.set_valeur_points_vie_courant(self.get_valeur_points_vie_courant()- autre_personnage.get_valeur_force())


#### Classe pour gérer le menu déroulant ####
class MenuDeroulant:
    # Constructeur de la classe
    def __init__(self, x, y, largeur, hauteur, options):
        self.rect = pygame.Rect(x, y, largeur, hauteur) # On crée un rectangle aux coordonnées (x,y)
        self.options = options # Liste de chaines de caractère contenant les options disponibles
        self.ouvert = False  # Le menu déroulant est fermé initialement
        self.selection = "ring1" # Par défaut on choisit le ring1 
        self.titre = "Choisir une arène" # titre affiché sur le Menu

    # Méthode permettant d'afficher le menu à l'écran
    def afficher(self, fenetre):
        """Dans la méthode afficher, Il y a deux paramètres « self » et  "Écran" qui représente l’affichage du programme avec tous les éléments graphique qui ont été codé. 
        La méthode sert à afficher le menu du jeu qu’on a codé, tout en utilisant certains aspect du programme pour voir la taille, le contenu du menu
        """
        #Affichage du rectangle de couleur de la taille et des coordonnées définie dans l'attribut rect
        pygame.draw.rect(fenetre, noir, self.rect)
        #Récupération du titre dans l'attribut titre et création du texte
        texte_menu = police.render(self.titre, True, blanc)
        #Affichage du texte aux mêmes coordonnées que le rectangle + 10 (permet de le centrer)
        fenetre.blit(texte_menu, (self.rect.x + 10, self.rect.y + 10))

        #Si le menu est ouvert (donc si l'utilisateur a cliqué dessus)
        if self.ouvert:
            for i in range(len(self.options)):
                y_offset = (i + 1) * 50  # Espacement vertical entre les options
                #On construit un rectangle espacé avec le bon décalage et de taille 30
                option_rect = pygame.Rect(self.rect.x, self.rect.y + y_offset, self.rect.width, 50)
                #On affiche le rectangle à l'écran avec la couleur souhaitée
                pygame.draw.rect(fenetre, bleu, option_rect)
                #On crée le texte affiché sur le rectangle d'option
                texte_option = police.render(self.options[i], True, blanc)
                #On affiche le texte aux même coordonnées que le rectangle + 10
                fenetre.blit(texte_option, (option_rect.x, option_rect.y ))

    # Méthode qui détecte le clic sur le menu et va l'ouvrir pour afficher les options
    def gerer_clic(self, event):
        #Détection du clic sur le menu déroulant
        if event.type == pygame.MOUSEBUTTONDOWN:
            if self.rect.collidepoint(event.pos):
                #Si le menu est ouvert, il le ferme alors que s'il est fermé il l'ouvre
                self.ouvert = not self.ouvert
            #Si le menu est ouvert
            elif self.ouvert:
                for i in range(len(self.options)):
                    #Recalcul des positions des rectangles d'options
                    y_offset = (i + 1) * 30
                    option_rect = pygame.Rect(self.rect.x, self.rect.y + y_offset, self.rect.width, 30)
                    #Détection du clic sur la position d'un des rectangles d'options
                    if option_rect.collidepoint(event.pos):
                        #Affectation de l'option sélectionnée dans l'attribut sélection
                        self.selection = self.options[i]
                        #Fermeture du menu déroulant
                        self.ouvert = False
                        #Le titre du menu déroulant devient l'option sélectionnée
                        self.titre= self.selection


#### Fonction qui gère la page d'accueil de l'interface graphique ####
def accueil_combat():
    """La fonction accueil_combat permet d’afficher l'écran d'accueil du jeu. On charge une image de fond que l’on a choisi. Avec dedans le bouton « start » et le menu deroulant pour nos choix. 
    Des que l’on clique sur le bouton start le jeu se lance avec l’arene que l’on aura choisi. 
    De plus dans le programme on utilise toujours la méthode gerer_clip pour gerer les actions de souris de l’utilisateur
    """
    #Chargement, redimension de l'image de fond de l'écran d'accueil
    fond_accueil = pygame.image.load("images/accueil.png").convert()
    fond_accueil = pygame.transform.scale(fond_accueil, (largeur, hauteur))

    #Définition du bouton start qui servira à lancer le jeu
    bouton_start = pygame.Rect(320, 200, 200, 50) #coordonnées en x, coordonnées en y, largeur, hauteur
    bouton_retour = pygame.Rect(0, 0, 100, 100)
    #Définition d'une instance de menu déroulant qui servira à choisir l'arene du jeu
    menu = MenuDeroulant(100, 300, 310, 50, arenes)

    continuer = True 
    while continuer:
        #Affichage du fond d'écran
        fenetre.blit(fond_accueil, (0, 0))
        retour = pygame.image.load("images/retour1.png").convert_alpha()
        retour = pygame.transform.scale(retour, (100, 100))
        fenetre.blit(retour, (0,0))
        #Affichage du rectangle du bouton start
        pygame.draw.rect(fenetre, noir, bouton_start)

        #Affichage du texte du bouton start
        texte_start = police.render("Start", True, blanc)
        fenetre.blit(texte_start, (380, 205))

        #Affichage du menu déroulant des arènes
        menu.afficher(fenetre)

        #Récupération de tous les évènements captés par la fenêtre
        for event in pygame.event.get():

            #Si l'utilisateur a cliqué sur la croix
            if event.type == pygame.QUIT:
                #Fermeture de la fenêtre en quittant pygame
                continuer = False
                pygame.quit()

            #Si l'utilisateur a cliqué dans la fenêtre
            if event.type == pygame.MOUSEBUTTONDOWN:
                #Si le clic a été fait à la position du bouton_start
                if bouton_start.collidepoint(event.pos):
                    #On lance le jeu avec comme paramètre l'arêne qui a été choisie
                    return jeu(menu.selection)
            
            if event.type == pygame.MOUSEBUTTONDOWN:
                    if bouton_retour.collidepoint(event.pos):
                        return mj_accueil()

            #Lancement de la méthode gérer clic qui va capter les évènement de clic sur les options du menu
            menu.gerer_clic(event)

        #Actualisation de la fenêtre (obligatoire c'est ce qui actualise l'affichage de la fenêtre)
        pygame.display.flip()

#### Fonction qui lance la fenêtre du jeu ####
def jeu(arene):
    """La fonction jeu permet tout simplement de gerer le jeu. On a mis en paramètre "arene", qui spécifie le nom de l'arène où l’on joue dessus. On utilise une fonction récurrente dans la méthode pour faire tourner le jeux lorsque l’on l’utilise.
      La fonction gere aussi les clics de souris et les touches qui sont utilisé par l’utilisateur pour jouer à travers les personnages. 
      Elle affiche aussi les points et barres de vie des personnages, à la fin quand un des deux personnages n'a plus de hp on affiche un écran de fin avec un bouton pour recommencer
    """
    #On précise que ce sont des variables globales car elle ne sont pas dans la fonction
    global pos_brute1,pos_brute2, pv_courant1, pv_courant2, tour_personnage
    
    Personnage1 = Personnage("Combattant1",(100, 200),  "images/brute4.png" , 250, 250, randint(20,50))
    Personnage2 = Personnage("Combattant2",(500, 200),  "images/brute3.png", 250, 250, randint(20,50))
    
    #Définition des coordonnées de départ de la barre de vie du personnage 1
    x_barre1 = 100
    y_barre1 = 560
    
    # pareil personnage 2 
    x_barre2=550
    y_barre2=560


    #Boucle du jeu
    continuer = True
    while continuer:

        #Chargement et redimension de l'image de fond d'écran dont le nom est passé en paramètre
        fond = pygame.image.load("images/"+arene+".png").convert()
        fond = pygame.transform.scale(fond, (largeur, hauteur))

        #Affichage de l'image de fond  
        fenetre.blit(fond, (0, 0))
        #Affichage des personnages
        fenetre.blit(Personnage1.get_image(), Personnage1.get_position_ecr())
        fenetre.blit(Personnage2.get_image(), Personnage2.get_position_ecr())

        #Récupération de tous les évènements captés par la fenêtre
        for event in pygame.event.get():
            #Si l'utilisateur a cliqué sur la croix
            if event.type == pygame.QUIT:
                #Fin de la boucle infinie et le jeu se termine
                pygame.quit()
            # Si c'est au personnage 1 de jouer et que l'utilisateur a tapé sur une touche
            if tour_personnage == 1 and event.type == pygame.KEYDOWN:
                # Si la touche était la barre d'espace
                if event.key == pygame.K_SPACE:
                    # Le personnage 1 attaque le personnage 2, cela réduit les pv du personnage 2 de 20
                    Personnage1.frappe(Personnage2, randint(20, 50))

                
                    #Déplacement de l'image du personnage 1 vers le personnage 2
                    Personnage1.set_position_ecr((500, 200))
                    #Ré-affichage de l'écran
                    fenetre.blit(fond, (0, 0))
                    #Ré-affichage des personnages
                    fenetre.blit(Personnage1.get_image(), Personnage1.get_position_ecr())
                    fenetre.blit(Personnage2.get_image(), Personnage2.get_position_ecr())
                    #Re-déplacement du personnage 1 à sa position initiale
                    Personnage1.set_position_ecr((100,200))

                    #Le tour passe au prochain personnage
                    tour_personnage = 3 - tour_personnage
                    pygame.time.wait(200)


            #Même chose pour le personnage 2
            elif tour_personnage == 2 and event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    Personnage2.frappe(Personnage1, randint(20, 50))
                    Personnage2.set_position_ecr((100, 200))
                    fenetre.blit(fond, (0, 0))
                    fenetre.blit(Personnage1.get_image(),  Personnage1.get_position_ecr())
                    fenetre.blit(Personnage2.get_image() ,  Personnage2.get_position_ecr())
                    Personnage2.set_position_ecr((500, 200))
                    tour_personnage = 3 - tour_personnage
                    pygame.time.wait(200)

        #Affichage des noms des personnages
        fenetre.blit(Personnage1.get_nom(), (100, 490))
        fenetre.blit(Personnage2.get_nom(),(550,490))

        #Affichage des pv des personnages
            #Création de la chaine de caractère
        pv1= str(Personnage1.get_valeur_points_vie_courant())+"/"+str(Personnage1.get_valeur_points_vie_initial())
        pv2= str(Personnage2.get_valeur_points_vie_courant())+"/"+str(Personnage2.get_valeur_points_vie_initial())
            #Création du texte
        texte_pv1 = police.render(pv1, True, rouge)
        texte_pv2 = police.render(pv2, True, rouge)
            #Affichage du texte à l'écran
        fenetre.blit(texte_pv1,(100,525))
        fenetre.blit(texte_pv2,(550,525))

        #Affichage des barres de vie des personnages
            #Calcul de la largeur de la barre en pourcentage par rapport à son nombre de pv
        largeur_barre_actuelle1 = (Personnage1.get_valeur_points_vie_courant()/ 100) * largeur_barre_max
        largeur_barre_actuelle2 = (Personnage2.get_valeur_points_vie_courant()/ 100) * largeur_barre_max
            #Affichage du rectangle de barre de vie à l'écran
        pygame.draw.rect(fenetre, vert, (x_barre1, y_barre1, largeur_barre_actuelle1, hauteur_barre))
        pygame.draw.rect(fenetre, vert, (x_barre2, y_barre2, largeur_barre_actuelle2, hauteur_barre))

        #Lorsque l'un des deux personnages a ses pv égaux ou inférieurs à 0 alors le jeu s'arrête
        if Personnage1.get_valeur_points_vie_courant() <= 0 or Personnage2.get_valeur_points_vie_courant()  <=0:
            if Personnage1.get_valeur_points_vie_courant() <= 0:
                rejouer1=True
                while rejouer1==True:
                    # page de fin avec joueur 1 gagnant
                    fond_fin=pygame.image.load("images/podium.png ").convert()
                    fond_fin= pygame.transform.scale(fond_fin, (largeur, hauteur))
                    fenetre.blit(fond_fin, (0, 0))
                    fenetre.blit(Personnage2.get_nom(), (380, 350))
                    fenetre.blit(Personnage2.get_image(), (330, 50))
                    bouton_restart = pygame.Rect(300, 500, 350, 50) #coordonnées en x, coordonnées en y, largeur, hauteur
                    pygame.draw.rect(fenetre, noir, bouton_restart)
                    texte_start = police.render("Relancer une partie", True, blanc)
                    fenetre.blit(texte_start, (300, 515))
                    
                    for event in pygame.event.get():
                        if event.type == pygame.MOUSEBUTTONDOWN:
                            if bouton_restart.collidepoint(event.pos):
                                # on revient à accueil mini jeu
                                mj_accueil()
                                 
                    pygame.display.flip()
                    
            elif Personnage2.get_valeur_points_vie_courant() <= 0:
                rejouer=True
                while rejouer==True:
                    # page fin avec joueur 2 gagnant 
                    fond_fin=pygame.image.load("images/podium.png").convert()
                    fond_fin= pygame.transform.scale(fond_fin, (largeur, hauteur))
                    fenetre.blit(fond_fin, (0, 0))
                    fenetre.blit(Personnage1.get_nom(), (380, 350))
                    fenetre.blit(Personnage1.get_image(), (330, 50))
                    bouton_restart = pygame.Rect(300, 500, 350, 50) #coordonnées en x, coordonnées en y, largeur, hauteur
                    pygame.draw.rect(fenetre, noir, bouton_restart)
                    texte_start = police.render("Relancer une partie", True, blanc)
                    fenetre.blit(texte_start, (300, 515))
                    
                    for event in pygame.event.get():
                        if event.type == pygame.MOUSEBUTTONDOWN:
                            if bouton_restart.collidepoint(event.pos):
                                # on revient à accueil mini jeu
                                 mj_accueil()
                                 
                    pygame.display.flip()
                    continuer = True
                    
        #Actualisation de l'affichage 
        pygame.display.flip()
        

#début du programme
accueil()
#fermer pygame à la fin 
pygame.quit()