#importation modules
import pygame
import sys
import os

# Si vous encontrez un problème de connexion entre les modules modifiez la ligne ci-dessous avec l'adresse du répertoire où vous avez enregistré le projet 
#os.chdir("U:\Documents\projet_concours\projet concours")
pygame.init()

# Paramètres liés à la fenêtre
(largeur, hauteur) = (850, 600)
fenetre = pygame.display.set_mode((largeur, hauteur))
pygame.display.set_caption("Quiz Game")
background_image = "images/image_fond.png"

# polices et couleurs 
bolice = pygame.font.Font(None, 80)
police = pygame.font.Font(None, 25)
couleur_texte = (255, 255, 255)
couleur_bouton = (100, 100, 100, 128)



def display_question(question, background_image, score):
    """
    c'est cette fonction qui va afficher les questions dans la fenetre ainsi que le calcul du score sous forme de texte 
    """
    fond = pygame.image.load("images/page1.png").convert()
    fond = pygame.transform.scale(fond, (largeur, hauteur))
    fenetre.blit(fond, (0,0))
    

    # Affichage du texte de la question
    texte_question = police.render(question.question, True, couleur_texte)
    fenetre.blit(texte_question, (50, 50))
    
    # Affichage du score
    texte_score = police.render("Score: " + str(score), True, couleur_texte)
    fenetre.blit(texte_score, (50, 20))


    # creation des boutons de réponse 
    bouton_A = pygame.Rect(50, 200, 700, 50)
    bouton_B = pygame.Rect(50, 300, 700, 50)
    bouton_C = pygame.Rect(50, 400, 700, 50)
    bouton_D = pygame.Rect(50, 500, 700, 50)
    
    # affichage des boutons de reponses 
    pygame.draw.rect(fenetre, couleur_bouton, bouton_A)
    pygame.draw.rect(fenetre, couleur_bouton, bouton_B)
    pygame.draw.rect(fenetre, couleur_bouton, bouton_C)
    pygame.draw.rect(fenetre, couleur_bouton, bouton_D)

    # affichage des texte des reponses sur les rectangles 
    texte_A = police.render(str(question.reponses[0]), True, couleur_texte)
    fenetre.blit(texte_A, (bouton_A.x + 10, bouton_A.y + 10))

    texte_B = police.render(str(question.reponses[1]), True, couleur_texte)
    fenetre.blit(texte_B, (bouton_B.x + 10, bouton_B.y + 10))

    texte_C = police.render(str(question.reponses[2]), True, couleur_texte)
    fenetre.blit(texte_C, (bouton_C.x + 10, bouton_C.y + 10))

    texte_D = police.render(str(question.reponses[3]), True, couleur_texte)
    fenetre.blit(texte_D, (bouton_D.x + 10, bouton_D.y + 10))
    
    
    pygame.display.flip()

    return bouton_A, bouton_B, bouton_C, bouton_D, score
    




def page_fin(background_image, score):
    """
    page qui affiche le score et permet de revenir en arriere 
    """
    bouton_retour = pygame.Rect(0, 0, 100, 100)
    retour = pygame.image.load("images/retour1.png").convert_alpha()
    retour = pygame.transform.scale(retour, (100, 100))
    fond_score = pygame.image.load("images/page1.png").convert()
    fond_score = pygame.transform.scale(fond_score, (largeur, hauteur))
    texte_score = bolice.render(f"Score: {score}", True, couleur_texte)
    
    continuer = True
    while continuer:
        for event in pygame.event.get():
            # clic sur la croix 
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
                
            if event.type == pygame.MOUSEBUTTONDOWN:
                    # clic sur le bouton de retour 
                    if bouton_retour.collidepoint(event.pos):
                        return 0
                        
        fenetre.blit(fond_score,(0,0))
        fenetre.blit(texte_score, (330, 300))
        fenetre.blit(retour, (0,0))
        pygame.display.flip()
        
        


def quiz(questions, image_fond=None):
    """
    page principale du quiz avec les evenements de clic 
    """
    
    score = 0  # Initialisez le score à zéro
    
    # affichage des questions et bouton et affichage du score 
    for i, question in enumerate(questions, 1):
        bouton_A, bouton_B, bouton_C, bouton_D, texte_score = display_question(question, image_fond, score)

        continuer = True
        reponse_utilisateur = ''  # Initialiser reponse_utilisateur
        while continuer:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                
                # ici on recupere les clic sur les boutons, dès que le joueur a choisi un bouton ça passe à la suite
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if bouton_A.collidepoint(event.pos):
                        reponse_utilisateur = 'A'
                        continuer = False
                    elif bouton_B.collidepoint(event.pos):
                        reponse_utilisateur = 'B'
                        continuer = False
                    elif bouton_C.collidepoint(event.pos):
                        reponse_utilisateur = 'C'
                        continuer = False
                    elif bouton_D.collidepoint(event.pos):
                        reponse_utilisateur = 'D'
                        continuer = False
                        
            pygame.display.flip()
            
        # on regarde si la reponse choisie était la bonne 
        if reponse_utilisateur == question.reponse_correcte:
            score += 1  # Incrémente le score uniquement pour les réponses correctes
            #print("Question"+ str(i) +"/"+str(len(questions)) + ": Correct")
        #else:
            #print("Question"+ str(i) +"/"+str(len(questions)) + ": Incorrect")
    
    page_fin(background_image, score)
                



