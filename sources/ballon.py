#importation modules
import pygame
import random
import os

# Si vous encontrez un problème de connexion entre les modules modifiez la ligne ci-dessous avec l'adresse du répertoire où vous avez enregistré le projet 
#os.chdir("U:\Documents\projet_concours\projet concours")

pygame.init()

(largeur, hauteur) = (800, 600)

#Création de la fenêtre aux bonnes dimensions
fenetre = pygame.display.set_mode((largeur, hauteur))

noir = (0, 0, 0)

#Définition du nom de la fenêtre 
pygame.display.set_caption("Foot")

#On charge les images et on définit une position de départ sous forme de tuple
gardien = pygame.image.load("images/gardienface.png").convert_alpha()
pos_gardien = (325,250)

ballon = pygame.image.load("images/ballon.png").convert_alpha()
pos_ballon = (325,500)

#Variables globales
choix_joueur = None
tour_joueur = True
score_joueur = 0
score_gardien = 0

#Police pour écrire du texte
police=pygame.font.Font(None, 24)

# Fonction qui renvoie l'issue du duel en fonction du choix du joueur et du choix aléatoire du gardien
def duel(choix_joueur, score_joueur, score_gardien ):
    choix_gardien = random.choice(['gauche','droite','centre'])
    

    if choix_gardien == 'centre':
        pos_gardien = (350,250)
        if choix_joueur == 'centre':
            # s'ils ont tous les deux choisi centre, le gardien rattrape le ballon et marque 1 point 
            gardien = pygame.image.load("images/gardiencentre.png").convert_alpha()
            score_gardien=score_gardien+1
        else:
             # sinon le gardien rate le ballon et le joueur gagne 1 point
            gardien = pygame.image.load("images/gardienface.png").convert_alpha()
            score_joueur = score_joueur+1


    elif choix_gardien == 'gauche':
        pos_gardien = (200,250)
        if choix_joueur =='gauche':
             # s'ils ont tous les deux choisi gauche, le gardien rattrape le ballon
            gardien = pygame.image.load("images/gardiengauche.png").convert_alpha()
            score_gardien=score_gardien+1
        else:
            # sinon le gardien rate le ballon et le joueur gagne 1 point
            gardien = pygame.image.load("images/gardiengaucherate.png").convert_alpha()
            score_joueur = score_joueur+1

    elif choix_gardien == 'droite':
        pos_gardien = (500,250)
        if choix_joueur =='droite':
            gardien = pygame.image.load("images/gardiendroite.png").convert_alpha()
            score_gardien=score_gardien+1
        else:
            gardien = pygame.image.load("images/gardiendroiterate.png").convert_alpha()
            score_joueur = score_joueur+1

    return gardien, pos_gardien , score_joueur, score_gardien


#### Fonction qui gère la page d'accueil de l'interface graphique ####
def fenetre_accueil():
    global pos_gardien, gardien, ballon, pos_ballon, choix_joueur, tour_joueur, score_joueur, score_gardien

   
    #Chargement, redimension de l'image de fond de l'écran d'accueil
    fond_accueil = pygame.image.load("images/cages2.png").convert()
    fond_accueil = pygame.transform.scale(fond_accueil, (largeur, hauteur))

    #Boucle de la page d'accueil qui continue tant qu'on ne clique pas sur la croix
    continuer = True
    
    #variables pour gérer le temps de rafraichissement de l'affichage 
    temp_de_refresh=50000
    compteur=0
    score_maxi=10

    while continuer:
        
        # On décompte jusqu'au rafraichissement 
        compteur=compteur+1
        if compteur>temp_de_refresh:
            compteur=0
            
            # Affichage des images et texte 
            fenetre.blit(fond_accueil, (0, 0))
            fenetre.blit(gardien, pos_gardien)
            fenetre.blit(ballon, pos_ballon)
            text = police.render("Score joueur : "+str(score_joueur),1,(255,255,255))
            text2 = police.render("Score gardien : "+str(score_gardien),1,(255,255,255))
            fenetre.blit(text, (500, 20))
            fenetre.blit(text2, (180, 20))
            
            # Fin : si le joueur ou le gardien gagne 
            if score_joueur==score_maxi:
                fin = fenetre_de_fin("Victoire")
                
                # On remet tout à zero 
                gardien = pygame.image.load("images/gardienface.png").convert_alpha()
                pos_gardien = (325,250)
                pos_ballon = (325,500)
                choix_joueur = None
                tour_joueur = True
                score_joueur = 0
                score_gardien = 0
                return fin
                
            if score_gardien==score_maxi:
                fin = fenetre_de_fin("Perdu")
                gardien = pygame.image.load("images/gardienface.png").convert_alpha()
                pos_gardien = (325,250)
                pos_ballon = (325,500)
                choix_joueur = None
                tour_joueur = True
                score_joueur = 0
                score_gardien = 0
                return fin

    
    
            #Récupération de tous les évènements captés par la fenêtre
            for event in pygame.event.get():
                #Si l'utilisateur a cliqué sur la croix
                if event.type == pygame.QUIT:
                    # on sort de la boucle
                    continuer = False
                if event.type == pygame.MOUSEBUTTONDOWN:
                    # S'il y a un clic sur le bouton gauche, le joueur a choisi de tirer à gauche
                    if event.button == 1 :
                        if tour_joueur == True:
                            gardien, pos_gardien, score_joueur, score_gardien = duel('gauche', score_joueur, score_gardien)
                            choix_joueur = 'gauche'
                            tour_joueur = False


    
                    if event.button == 2 :
                        
                        if tour_joueur == True:
                            gardien, pos_gardien, score_joueur, score_gardien = duel('centre', score_joueur, score_gardien)
                            choix_joueur = 'centre'
                            tour_joueur = False
    
    
    
                    if event.button == 3 :
                        if tour_joueur == True:
                            gardien, pos_gardien, score_joueur, score_gardien = duel('droite', score_joueur, score_gardien)
                            choix_joueur = 'droite'
                            tour_joueur = False
    
    
            if choix_joueur == 'centre':
                
                # On déplace progressivement le ballon au centre
                if pos_ballon[0] >  350 :
                    pos_ballon = pos_ballon[0]-1 , pos_ballon[1]
    
                if pos_ballon[1] > 300 :
                    pos_ballon = pos_ballon[0] , pos_ballon[1]-1
                # Dès qu'il a atteint le point voulu, on le remet à sa pos de départ, on remet tout à zero
                if pos_ballon[0] == 350  and pos_ballon[1] == 300:
                    pos_ballon = (350,500)
                    choix_joueur = None
                    tour_joueur = True
                    gardien = pygame.image.load("images/gardienface.png").convert_alpha()
                    pos_gardien = (350,250)
                   
    
    
            if choix_joueur == 'gauche':
               
                if pos_ballon[0] >  150 :
                    pos_ballon = pos_ballon[0]-1 , pos_ballon[1]
    
                if pos_ballon[1] > 300 :
                    pos_ballon = pos_ballon[0] , pos_ballon[1]-1
    
                if pos_ballon[0] == 150  and pos_ballon[1] == 300:
                   
                    pos_ballon = (350,500)
                    choix_joueur = None
                    tour_joueur = True
                    gardien = pygame.image.load("images/gardienface.png").convert_alpha()
                    pos_gardien = (350,250)
                   
    
            if choix_joueur == 'droite':
                if pos_ballon[0] <  550 :
                    pos_ballon = pos_ballon[0]+1 , pos_ballon[1]
    
                if pos_ballon[1] > 300 :
                    pos_ballon = pos_ballon[0] , pos_ballon[1]-1
    
                if pos_ballon[0] == 550  and pos_ballon[1] == 300:
                    pos_ballon = (350,500)
                    choix_joueur = None
                    tour_joueur = True
                    gardien = pygame.image.load("images/gardienface.png").convert_alpha()
                    pos_gardien = (350,250)
                    
    
            

    
            #Actualisation de la fenêtre
            pygame.display.flip()

def fenetre_de_fin(resultats):
    
    bouton_retour = pygame.Rect(0, 0, 100, 100)
    text = police.render(resultats,1,(255,255,255))
    fond_accueil = pygame.image.load("images/cages2.png").convert()
    fond_accueil = pygame.transform.scale(fond_accueil, (largeur, hauteur))
    

    #Boucle de la page de fin qui continue tant qu'on ne clique pas sur la croix
    continuer = True
    while continuer:
        
        # Affichage du fond du texte et du bouton 
        retour = pygame.image.load("images/retour1.png").convert_alpha()
        retour = pygame.transform.scale(retour, (100, 100))
        fenetre.blit(fond_accueil, (0, 0))
        fenetre.blit(text, (350, 100))
        fenetre.blit(retour, (0,0))
        
        #Récupération de tous les évènements captés par la fenêtre
        for event in pygame.event.get():
            #Si l'utilisateur a cliqué sur la croix
            if event.type == pygame.QUIT:
                pygame.quit()
                # on sort de la boucle
                continuer = False
            if event.type == pygame.MOUSEBUTTONDOWN:
                    if bouton_retour.collidepoint(event.pos):
                        return 0 
        #Actualisation de la fenêtre
        pygame.display.flip()

#fenetre_accueil()
#pygame.quit()